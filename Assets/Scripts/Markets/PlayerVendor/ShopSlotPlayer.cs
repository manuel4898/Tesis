﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSlotPlayer : ShopSlot
{
    public bool validToBuyFrom;
    public int amountToSell; 
    protected override void Start()
    {
        amountToBuy = 0;
        id = -1;
        price = -1;
        shopActive = false;
        validToBuyFrom = false;
        stockText = this.transform.GetChild(1).GetChild(2).GetComponent<Text>();
        this.transform.GetChild(1).GetChild(3).GetComponent<InputField>().onValueChanged.AddListener((data) => AmountToSell(data));
        this.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<InputField>().onValueChanged.AddListener((data) => PriceEntered(data));
        this.transform.GetChild(1).GetChild(4).GetChild(0).GetComponent<Button>().onClick.AddListener(this.PutOnSale);
        this.transform.GetChild(1).GetChild(4).GetChild(0).GetComponent<Image>().color = Color.gray;
    }
    override public void OnNewDay()
    {
        if (validToBuyFrom)
        {
            SetIconAndName();
            this.transform.GetChild(1).GetChild(1).GetChild(0).GetChild(0).GetComponent<Text>().text = price.ToString();
            stockText.text = "Left: " + stock.ToString();
        }
    }
    public void SetIconAndName()
    {
        if (id < 0) return;
        this.transform.GetChild(0).GetComponent<Image>().sprite = items.ItemsData[id].icon;
        this.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = items.ItemsData[id].displayName;
    }
    public void NotValidToBuy()
    {
        validToBuyFrom = false;
        this.transform.GetChild(1).GetChild(4).GetChild(0).GetComponent<Image>().color = Color.white;
    }
    public void PriceEntered(string amount)
    {
        if (id < 0) return;
        NotValidToBuy();
        if (!float.TryParse(amount, out this.price)) this.price = 999;
        else if (this.price < 0) this.price = 999;

        price = Mathf.Round(price * 100) / 100f;
    }
    public void AmountToSell(string amount)
    {
        if (id < 0) return;
        NotValidToBuy();
        if (!int.TryParse(amount, out amountToSell)) amountToSell = 0;
        else if (amountToSell < 0) amountToSell = 0;

        if (amountToSell > inventario.inventory[id]) amountToSell = inventario.inventory[id];
    }
    public void PutOnSale()
    {
        if (id < 0 || price < 0 || price > 900) return;
        stock += amountToSell;
        amountToSell = 0;
        this.transform.GetChild(1).GetChild(3).GetComponent<InputField>().text = "";
        this.transform.GetChild(1).GetChild(1).GetChild(0).GetChild(1).GetComponent<Text>().text = price.ToString();
        stockText.text = "Left: " + stock.ToString();
        validToBuyFrom = true;
        this.transform.GetChild(1).GetChild(4).GetChild(0).GetComponent<Image>().color = Color.gray;
    }
}
