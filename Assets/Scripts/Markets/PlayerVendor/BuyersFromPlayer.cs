﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyersFromPlayer : BuyersGroups
{
    protected override void Update()
    {
        if (shopActive)
        {
            buyersText.text = " Buyers: " + buyers.ToString();
            timer += Time.deltaTime;
            if (buyers > 0 && timer > bar && shopsTemp.Count > 0)
            {
                BuyAction();
                timer = 0;
            }
        }
    }
    protected override void BuyAction()
    {
        for (int j = 0; j < shops.Count; j++)
        {
            if ((shops[j] as ShopSlotPlayer).validToBuyFrom)
            {
                float chanceOfBuy = 0;
                float priceBase = items.ItemsData[shops[j].id].prices[89];
                if (shops[j].price < 2 * priceBase)
                {
                    chanceOfBuy = 1 - shops[j].price / (2 * priceBase);
                }
                Mathf.Clamp01(chanceOfBuy);
                if (shops[j].stock > 0 && shops[j].inventario.inventory[shops[j].id] > 0)
                {
                    float decideToBuy = Random.Range(0f, 1f);
                    if (decideToBuy < chanceOfBuy)
                    {
                        shops[j].Buy();
                        shops[j].inventario.inventory[shops[j].id]--;
                        shops[j].inventario.money += shops[j].price;
                    }
                }
            }
        }
        float chanceToBuyMore = Random.Range(0f, 1f);
        if (chanceToBuyMore < 0.5f)
            buyers--;
    }
}
