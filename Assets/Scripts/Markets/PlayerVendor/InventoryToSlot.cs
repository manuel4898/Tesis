﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryToSlot : MonoBehaviour, IPointerDownHandler
{
    public Items items;
    public Inventory inventario;
    public int id;
    public ShopSlotPlayer slotThatCalled;
    void Start()
    {
        this.GetComponent<Image>().sprite = items.ItemsData[id].icon;
        this.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = inventario.inventory[id].ToString();
    }
    void Update()
    {
        this.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = inventario.inventory[id].ToString();
    }
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        slotThatCalled.id = id;
        slotThatCalled.SetIconAndName();
        transform.parent.parent.gameObject.SetActive(false);
    }
}
