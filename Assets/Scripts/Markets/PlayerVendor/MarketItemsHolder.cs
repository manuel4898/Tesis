﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketItemsHolder : MonoBehaviour
{
    public InventoryToSlot[] itemsInventory;

    public ShopSlotPlayer slotThatCalled;
    [SerializeField] Items items;
    [SerializeField] Inventory inventario;
    private void Awake()
    {
        itemsInventory = this.transform.GetComponentsInChildren<InventoryToSlot>();
        for (int i = 0; i < itemsInventory.Length; i++)
        {
            itemsInventory[i].id = i;
            itemsInventory[i].items = items;
            itemsInventory[i].inventario = inventario;
        }
    }
    private void OnEnable()
    {
        itemsInventory = this.transform.GetComponentsInChildren<InventoryToSlot>();
        for (int i = 0; i < itemsInventory.Length; i++)
        {
            itemsInventory[i].slotThatCalled = slotThatCalled;
        }
    }
}
