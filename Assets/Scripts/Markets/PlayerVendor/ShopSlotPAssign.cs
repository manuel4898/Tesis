﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopSlotPAssign : MonoBehaviour, IPointerDownHandler
{
    public GameObject inventoryToSellObj;
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        this.transform.parent.GetComponent<ShopSlotPlayer>().NotValidToBuy();
        this.transform.GetChild(0).GetComponent<Text>().text = "";
        inventoryToSellObj.transform.GetChild(2).GetComponent<MarketItemsHolder>().slotThatCalled = this.transform.parent.GetComponent<ShopSlotPlayer>();
        inventoryToSellObj.SetActive(true);

    }
}
