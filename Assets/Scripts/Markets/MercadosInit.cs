﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MercadosInit : MonoBehaviour
{
    public Button onEnableButton;

    public void OnStart()
    {
        onEnableButton.onClick.Invoke();
    }
}
