﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyersGroups : MonoBehaviour
{
    public DayManager dayManager;
    public int reputation;
    public List<ShopSlot> shops;
    public Items items;
    protected List<int> demanda;
    public int buyers;
    public Text buyersText;
    protected bool shopActive;

    protected List<ShopSlot> shopsTemp;
    public float timer;
    public float bar;
    protected virtual void Start()
    {
        shopActive = false;
        ResetDemanda();
    }

    protected virtual void Update()
    {
        if (shopActive)
        {
            buyersText.text = " Buyers: " + buyers.ToString();
            timer += Time.deltaTime;
            if (buyers > 0 && timer > bar && shopsTemp.Count > 0)
            {
                BuyAction();
                timer = 0;
            }
        }
    }

    protected virtual void BuyAction()
    {
        bool[] compras = new bool[4];
        for (int i = 0; i < 4; i++)
        {
            int id = demanda[Random.Range(0, demanda.Count)];
            int lowerPriceShopIndex = -1;
            float lowerPrice = 9999f;

            for (int j = 0; j < shopsTemp.Count; j++)
            {
                if (shopsTemp[j].id == id && shopsTemp[j].price < lowerPrice && shopsTemp[j].stock > 0)
                {
                    lowerPrice = shopsTemp[j].price;
                    lowerPriceShopIndex = j;
                }
            }
            if (shopsTemp.Count > 0 && lowerPriceShopIndex != -1)
            {
                compras[i] = shopsTemp[lowerPriceShopIndex].Buy();
                if (shopsTemp[lowerPriceShopIndex].stock <= 0) shopsTemp.RemoveAt(lowerPriceShopIndex);
            }
        }
        if (compras[0] || compras[1] || compras[2] || compras[3])
            buyers--;
    }

    public void ShopOn()
    {
        shopActive = true;
        buyers = buyers / (dayManager.stagesIndex + 1);
        foreach (ShopSlot shop in shops)
        {
            shop.ShopOn();
        }
    }
    public void ShopOff()
    {
        shopActive = false;
        foreach (ShopSlot shop in shops)
        {
            shop.ShopOff();
        }
    }
    public void OnNewDay()
    {
        buyers = Mathf.RoundToInt(Random.Range(reputation * reputation * 0.75f, reputation * reputation * 1.25f));
        bar = 5 / Random.Range(buyers / 32f, buyers / 28f);
        shopsTemp = new List<ShopSlot>();
        shopsTemp.AddRange(shops);
        ResetDemanda();
    }

    public void ResetDemanda()
    {
        demanda = new List<int>();
        for (int i = 0; i < 25; i++)
        {
            AddDemanda(i, Random.Range(1, 3));
        }
        List<int> removeId = new List<int>();
        foreach (int id in demanda)
        {
            bool isNotPresent = true;
            foreach (ShopSlot shop in shops)
            {
                if (shop.id == id) isNotPresent = false;
            }
            if (isNotPresent) removeId.Add(id);
        }
        foreach (int id in removeId)
        {
            demanda.Remove(id);
        }
        for (int i = 0; i < demanda.Count; i++)
        {
            int temp = demanda[i];
            int randomIndex = Random.Range(i, demanda.Count);
            demanda[i] = demanda[randomIndex];
            demanda[randomIndex] = temp;
        }
    }

    public void AddDemanda(int id, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            demanda.Add(id);
        }
    }
}
