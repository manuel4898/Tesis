﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Vendor
{
    public string name;
    public int[] products;
    public int[] avgStocks;
}
[System.Serializable]
public class VendorList
{
    public Vendor[] vendorList;
}
public class VendorManager : MonoBehaviour
{
    public PlayerAsVendor playerAsVendor;
    public DayManager dayManager;

    public Text money;
    public GameObject allMarket;
    public Transform allMarketOriginalPos;
    public GameObject shopBlocker;
    public BuyersGroups[] compradores;

    public Items items;
    public Inventory inventario;
    public Transform BuyersLists;
    public Transform StoreLists;
    [SerializeField] ShopSlot[] shopSlotsExternal;
    public Transform vendNamesTransform;
    Text[] vendorsText;

    public VendorList vendendores = new VendorList();
    public TextAsset textJSON;
    Vendor[] HoseVendors;
    Vendor[] VioletVendors;
    Vendor[] SarahVendors;
    Vendor[] selectedHoseVendors;
    Vendor[] selectedVioletVendors;
    Vendor[] selectedSarahVendors;
    List<Vendor> remainingVendors;

    bool shopActive;
    private void Start()
    {
        money.text = "";
        compradores = BuyersLists.GetComponentsInChildren<BuyersGroups>();
        shopSlotsExternal = StoreLists.GetComponentsInChildren<ShopSlot>();
        StoreLists.GetChild(0).GetComponent<MoverTienda>().MoverTiendaButton();
        for (int i = 0; i < shopSlotsExternal.Length; i++)
        {
            shopSlotsExternal[i].inventario = inventario;
            shopSlotsExternal[i].items = items;
            compradores[i / 12].shops.Add(shopSlotsExternal[i]);
        }
        for (int i = 0; i < compradores.Length; i++)
        {
            compradores[i].reputation = 50 - 15 * i;
            compradores[i].dayManager = dayManager;
        }
        vendorsText = vendNamesTransform.GetComponentsInChildren<Text>();
        vendendores = JsonUtility.FromJson<VendorList>(textJSON.text);
        HoseVendors = new Vendor[5];
        VioletVendors = new Vendor[5];
        SarahVendors = new Vendor[5];
        selectedHoseVendors = new Vendor[3];
        selectedVioletVendors = new Vendor[3];
        selectedSarahVendors = new Vendor[3];
        remainingVendors = new List<Vendor>();
        for (int i = 0; i < 5; i++)
        {
            HoseVendors[i] = vendendores.vendorList[i];
            VioletVendors[i] = vendendores.vendorList[5 + i];
            SarahVendors[i] = vendendores.vendorList[10 + i];
        }
        OnNewDay();
    }
    private void Update()
    {
        if (shopActive) money.text = "Money: " + (Mathf.RoundToInt(inventario.money * (100)) / 100f);
    }
    public void ShopOn()
    {
        foreach (BuyersGroups buyers in compradores)
        {
            buyers.ShopOn();
        }
        shopActive = true;
    }
    public void ShopOff()
    {
        foreach (BuyersGroups buyers in compradores)
        {
            buyers.ShopOff();
        }
        shopActive = false;
    }
    public void OnNewDay()
    {
        ShopOff();
        //shopBlocker.SetActive(true);
        selectedHoseVendors = SelectVendors(HoseVendors);
        selectedVioletVendors = SelectVendors(VioletVendors);
        selectedSarahVendors = SelectVendors(SarahVendors);
        ChooseStocksForVendors();
        foreach (ShopSlot shop in shopSlotsExternal)
        {
            shop.OnNewDay();
        }
        foreach (BuyersGroups buyers in compradores)
        {
            buyers.OnNewDay();
        }
    }
    public void ChooseStocksForVendors()
    {
        int j = shopSlotsExternal.Length / 4;
        List<int> productsTemp = new List<int>();
        List<int> stockTemp = new List<int>();
        for (int i = 0; i < j / 4; i++)
        {
            productsTemp.AddRange(selectedHoseVendors[i].products);
            stockTemp.AddRange(selectedHoseVendors[i].avgStocks);
            for (int k = 0; k < 4; k++)
            {
                shopSlotsExternal[i * 4 + k].items = items;
                if (k + i == 0)
                {
                    shopSlotsExternal[0].id = productsTemp.Pop(0);
                    shopSlotsExternal[0].stock = Mathf.RoundToInt(Random.Range(0.75f * stockTemp[0], 1.25f * stockTemp[0]) * (1 + (items.ItemsData[shopSlotsExternal[0].id].oferta[items.month]) * 0.1f) / (dayManager.stagesIndex + 1));
                    float tempPrice = items.ItemsData[shopSlotsExternal[0].id].prices[89];
                    shopSlotsExternal[0].price = Mathf.Round((Random.Range(0.9f * tempPrice, 1.4f * tempPrice)) * 100f) / 100f;
                    stockTemp.RemoveAt(0);
                }
                else
                {
                    int rnd = Random.Range(0, productsTemp.Count);
                    shopSlotsExternal[i * 4 + k].id = productsTemp.Pop(rnd);
                    shopSlotsExternal[i * 4 + k].stock = Mathf.RoundToInt(Random.Range(0.75f * stockTemp[rnd], 1.25f * stockTemp[rnd]) * (1 + (items.ItemsData[shopSlotsExternal[i * 4 + k].id].oferta[items.month]) * 0.1f) / (dayManager.stagesIndex + 1));
                    float tempPrice = items.ItemsData[shopSlotsExternal[i * 4 + k].id].prices[89];
                    shopSlotsExternal[i * 4 + k].price = Mathf.Round((Random.Range(0.9f * tempPrice, 1.4f * tempPrice)) * 100f) / 100f;
                    stockTemp.RemoveAt(rnd);
                }
            }
            productsTemp.Clear();
            stockTemp.Clear();
        }
        for (int i = 0; i < j / 4; i++)
        {
            productsTemp.AddRange(selectedVioletVendors[i].products);
            stockTemp.AddRange(selectedVioletVendors[i].avgStocks);
            for (int k = 0; k < 4; k++)
            {
                shopSlotsExternal[j + i * 4 + k].items = items;
                if (k + i == 0)
                {
                    shopSlotsExternal[j].id = productsTemp.Pop(0);
                    shopSlotsExternal[j].stock = Mathf.RoundToInt(Random.Range(0.75f * stockTemp[0], 1.25f * stockTemp[0]) * (1 + (items.ItemsData[shopSlotsExternal[j].id].oferta[items.month]) * 0.1f) / (dayManager.stagesIndex + 1));
                    float tempPrice = items.ItemsData[shopSlotsExternal[j].id].prices[89];
                    shopSlotsExternal[j].price = Mathf.Round((Random.Range(0.75f * tempPrice, 1.4f * tempPrice)) * 100f) / 100f;
                    stockTemp.RemoveAt(0);
                }
                else
                {
                    int rnd = Random.Range(0, productsTemp.Count);
                    shopSlotsExternal[j + i * 4 + k].id = productsTemp.Pop(rnd);
                    shopSlotsExternal[j + i * 4 + k].stock = Mathf.RoundToInt(Random.Range(0.75f * stockTemp[rnd], 1.25f * stockTemp[rnd]) * (1 + (items.ItemsData[shopSlotsExternal[j + i * 4 + k].id].oferta[items.month]) * 0.1f) / (dayManager.stagesIndex + 1));
                    float tempPrice = items.ItemsData[shopSlotsExternal[j + i * 4 + k].id].prices[89];
                    shopSlotsExternal[j + i * 4 + k].price = Mathf.Round((Random.Range(0.75f * tempPrice, 1.4f * tempPrice)) * 100f) / 100f;
                    stockTemp.RemoveAt(rnd);
                }
            }
            productsTemp.Clear();
            stockTemp.Clear();
        }
        for (int i = 0; i < j / 4; i++)
        {
            productsTemp.AddRange(selectedSarahVendors[i].products);
            stockTemp.AddRange(selectedSarahVendors[i].avgStocks);
            for (int k = 0; k < 4; k++)
            {
                shopSlotsExternal[2 * j + i * 4 + k].items = items;
                if (k + i == 0)
                {
                    shopSlotsExternal[2 * j].id = productsTemp.Pop(0);
                    shopSlotsExternal[2 * j].stock = Mathf.RoundToInt(Random.Range(0.75f * stockTemp[0], 1.25f * stockTemp[0]) * (1 + (items.ItemsData[shopSlotsExternal[2 * j].id].oferta[items.month]) * 0.1f) / (dayManager.stagesIndex + 1)); 
                    float tempPrice = items.ItemsData[shopSlotsExternal[2 * j].id].prices[89];
                    shopSlotsExternal[2 * j].price = Mathf.Round((Random.Range(0.75f * tempPrice, 1.25f * tempPrice)) * 100f) / 100f;
                    stockTemp.RemoveAt(0);
                }
                else
                {
                    int rnd = Random.Range(0, productsTemp.Count);
                    shopSlotsExternal[2 * j + i * 4 + k].id = productsTemp.Pop(rnd);
                    shopSlotsExternal[2 * j + i * 4 + k].stock = Mathf.RoundToInt(Random.Range(0.75f * stockTemp[rnd], 1.25f * stockTemp[rnd]) * (1 + (items.ItemsData[shopSlotsExternal[2 * j + i * 4 + k].id].oferta[items.month]) * 0.1f) / (dayManager.stagesIndex + 1));
                    float tempPrice = items.ItemsData[shopSlotsExternal[2 * j + i * 4 + k].id].prices[89];
                    shopSlotsExternal[2 * j + i * 4 + k].price = Mathf.Round((Random.Range(0.75f * tempPrice, 1.25f * tempPrice)) * 100f) / 100f;
                    stockTemp.RemoveAt(rnd);
                }
            }
            productsTemp.Clear();
            stockTemp.Clear();
        }
        //for (int i = 0; i < j / 4; i++)
        //{
        //    productsTemp.AddRange(selectedSarahVendors[i].products);
        //    stockTemp.AddRange(selectedSarahVendors[i].avgStocks);
        //    for (int k = 0; k < 4; k++)
        //    {
        //        shopSlotsExternal[3 * j + i * 4 + k].items = items;
        //        if (k + i == 0)
        //        {
        //            shopSlotsExternal[3 * j].id = productsTemp.Pop(0);
        //            shopSlotsExternal[3 * j].stock = Mathf.RoundToInt(Random.Range(0.75f * stockTemp[0], 1.25f * stockTemp[0]) * (1 + (items.ItemsData[shopSlotsExternal[3 * j].id].oferta[items.month]) * 0.1f) / (dayManager.stagesIndex + 1));
        //            float tempPrice = items.ItemsData[shopSlotsExternal[3 * j].id].prices[89];
        //            shopSlotsExternal[3 * j].price = Mathf.Round((Random.Range(0.9f * tempPrice, 1.4f * tempPrice)) * 100f) / 100f;
        //            stockTemp.RemoveAt(0);
        //        }
        //        else
        //        {
        //            int rnd = Random.Range(0, productsTemp.Count);
        //            shopSlotsExternal[3 * j + i * 4 + k].id = productsTemp.Pop(rnd);
        //            shopSlotsExternal[3 * j + i * 4 + k].stock = Mathf.RoundToInt(Random.Range(0.75f * stockTemp[rnd], 1.25f * stockTemp[rnd]) * (1 + (items.ItemsData[shopSlotsExternal[3 * j + i * 4 + k].id].oferta[items.month]) * 0.1f) / (dayManager.stagesIndex + 1));
        //            float tempPrice = items.ItemsData[shopSlotsExternal[3 * j + i * 4 + k].id].prices[89];
        //            shopSlotsExternal[3 * j + i * 4 + k].price = Mathf.Round((Random.Range(0.9f * tempPrice, 1.4f * tempPrice)) * 100f) / 100f;
        //            stockTemp.RemoveAt(rnd);
        //        }
        //    }
        //    productsTemp.Clear();
        //    stockTemp.Clear();
        //}
    }
    public void ChangeVendorButton(int code)
    {
        switch (code)
        {
            case 0:
                { ChangeVendor(selectedHoseVendors); }
                break;
            case 1:
                { ChangeVendor(selectedVioletVendors); }
                break;
            case 2:
                { ChangeVendor(selectedSarahVendors); }
                break;
            case 3:
                { ChangeVendorPlayer(); }
                break;
            default:
                break;
        }
    }
    void ChangeVendor(Vendor[] vendors)
    {
        for (int i = 0; i < 3; i++)
        {
            vendorsText[i].text = vendors[i].name;
        }
    }
    void ChangeVendorPlayer()
    {
        for (int i = 0; i < 3; i++)
        {
            vendorsText[i].text = playerAsVendor.playerVendorsNames[i];
        }
    }

    Vendor[] SelectVendors(Vendor[] vendors)
    {
        List<Vendor> tempVendors = new List<Vendor>();
        Vendor[] tempSelectedVendors = new Vendor[3];
        tempSelectedVendors[0] = vendors[0];
        for (int j = 1; j < 5; j++)
        {
            tempVendors.Add(vendors[j]);
        }
        for (int i = 1; i < 3; i++)
        {
            int rnd = Random.Range(1, tempVendors.Count);
            tempSelectedVendors[i] = tempVendors[rnd];
            tempVendors.RemoveAt(rnd);
        }
        remainingVendors.AddRange(tempVendors);
        return tempSelectedVendors;
    }

    public void ButtonDeactivate()
    {
        allMarket.transform.position = new Vector3(3000, 0, 0);
    }
    public void ButtonActivate()
    {
        allMarket.transform.position = allMarketOriginalPos.position;
    }
}
