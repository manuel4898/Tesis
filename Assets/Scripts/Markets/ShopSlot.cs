﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSlot : MonoBehaviour
{
    public Inventory inventario;
    public Items items;
    public int id;
    public float price;
    public int stock;
    protected int amountToBuy;

    protected bool shopActive;
    protected Text stockText;
    protected virtual void Start()
    {
        amountToBuy = 0;
        shopActive = false;
        this.transform.GetChild(1).GetChild(3).GetComponent<InputField>().onValueChanged.AddListener((data) => ValueEntered(data));
        this.transform.GetChild(1).GetChild(4).GetChild(0).GetComponent<Button>().onClick.AddListener(this.BuyPlayer);
    }
    public virtual void OnNewDay()
    {
        stockText = this.transform.GetChild(1).GetChild(2).GetComponent<Text>();
        this.transform.GetChild(0).GetComponent<Image>().sprite = items.ItemsData[id].icon;
        this.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = items.ItemsData[id].displayName;
        this.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = "$ " + price.ToString();
        stockText.text = "Left: " + stock.ToString();
    }
    protected virtual void Update()
    {
        if (shopActive)
        {
            stockText.text = "Left: " + stock.ToString();
        }
    }

    public bool Buy()
    {
        if (stock <= 0) return false;
        stock--; return true;
    }

    public void BuyPlayer()
    {
        if (amountToBuy <= stock)
        {
            inventario.id = id;
            inventario.amount = amountToBuy;
            inventario.SetPrice(price);
            if (inventario.Buy()) stock -= amountToBuy;
        }
    }

    public void ShopOn()
    {
        shopActive = true;
    }
    public void ShopOff()
    {
        shopActive = false;
    }
    public virtual void ValueEntered(string amount)
    {
        if (!int.TryParse(amount, out this.amountToBuy)) this.amountToBuy = 0;
        else if (this.amountToBuy < 0) this.amountToBuy = 0;
    }

}
