﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySystem : MonoBehaviour
{
    private Dictionary<ItemData, Item> m_itemDictionary;
    public List<Item> inventory { get; private set; }
    private void Awake()
    {
        inventory = new List<Item>();
        m_itemDictionary = new Dictionary<ItemData, Item>();
    }
    public void Add(ItemData referenceData, int ammount)
    {
        if (m_itemDictionary.TryGetValue(referenceData, out Item value))
        {
            value.AddToStack(ammount);
        }
        else
        {
            Item newItem = new Item(referenceData, ammount);
            inventory.Add(newItem);
            m_itemDictionary.Add(referenceData, newItem);
        }
    }
    public void Remove(ItemData referenceData, int ammount)
    {
        if (m_itemDictionary.TryGetValue(referenceData, out Item value))
        {
            value.RemoveFromStack(ammount);
            if(value.stackSize < 1)
            {
                inventory.Remove(value);
                m_itemDictionary.Remove(referenceData);
            }
        }
    }
}
public class Item
{
    public ItemData data { get; private set; }
    public int stackSize { get; private set; }

    public Item(ItemData source)
    {
        data = source;
        AddToStack(1);
    }
    public Item(ItemData source, int ammount)
    {
        data = source;
        AddToStack(ammount);
    }
    public void AddToStack(int ammount)
    {
        stackSize += ammount;
    }
    public void RemoveFromStack(int ammount)
    {
        stackSize -= ammount;
    }
}
