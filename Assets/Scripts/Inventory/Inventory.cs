﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory")]
public class Inventory : ScriptableObject
{
    [SerializeField]Items items;
    public float money;
    public int[] inventory;
    public int[] shares;
    public int id;
    public int amount;

    public float tempPrice;
    public void Begin()
    {
        if (inventory.Length < 1 || shares.Length < 1)
        {
            inventory = new int[25];
            money = 1000f;
            shares = new int[25];
        }
        amount = 0;
        tempPrice = 0;
    }
    public void ValueEntered(string amount)
    {
        if (!int.TryParse(amount, out this.amount)) this.amount = 0;
        else if (this.amount < 0) this.amount = 0;
    }
    public void SetPrice(float price)
    {
        tempPrice = price;
    }
    public bool Buy()
    {
        if (amount * tempPrice <= money)
        {
            inventory[id] += amount;
            money -= amount * tempPrice;
            return true;
        }
        return false;
    }
    public void SellPlace()
    {
        if(amount <= inventory[id])
        {
            inventory[id] -= amount;;
        }
    }
    public void SellRemove()
    {
        if (amount <= inventory[id])
        {
            inventory[id] += amount;;
        }
    }
    public void BuyShares()
    {
        float temp = items.ItemsData[id].prices[89] * 1.05f;
        if (amount * temp <= money)
        {
            shares[id] += amount;
            money -= amount * temp;
        }
    }
    public void SellShares()
    {
        if (amount <= shares[id])
        {
            shares[id] -= amount;
            money += amount * items.ItemsData[id].prices[89] * 0.95f;
        }
    }
    public void Add(int _amount, int _id)
    {
        if (_id <= 24)
            inventory[_id] += _amount;
    }

    public void Remove(int _amount, int _id)
    {
        if (_id <= 24)
        {
            if (inventory[_id] < _amount) _amount = inventory[_id];
            inventory[_id] -= _amount;
        }
    }

}
