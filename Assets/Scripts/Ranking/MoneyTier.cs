﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyTier : MonoBehaviour
{
    public int[] topCapitals;
    public int[] originalTopCapitals;
    public string[] topNames;
    public string[] originaltopNames;

    public Transform grid;
    public Transform gridNames;
    public Inventory inventario;
    private void Init()
    {
        if (topCapitals.Length < 1)
        {
            originalTopCapitals = new int[] { 32000, 50000, 143000, 200000, 341000, 555555, 600320, 999998, 12345678, 98765432 };
            topCapitals = new int[originalTopCapitals.Length];

            originaltopNames = new string[] { "Holly", "Serena", "Sophie", "Benedict", "Mauricio", "Yue", "Viktor", "Mrs. Sarah", "Violet", "Don Hose" };
            topNames = new string[originaltopNames.Length];

            for (int i = 0; i < originalTopCapitals.Length; i++)
            {
                topCapitals[i] = originalTopCapitals[i];
                topNames[i] = originaltopNames[i];
            }
        }
    }
    private void OnEnable()
    {
        Init();
        InputScore(Mathf.RoundToInt(inventario.money));
        HighScore();
    }

    public void InputScore(int score)
    {
        bool placeLocated = false;
        for (int i = 0; i < originalTopCapitals.Length; i++)
        {
            topCapitals[i] = originalTopCapitals[i];
            topNames[i] = originaltopNames[i];
        }
        for (int i = 9; i > 0; i--)
        {
            if (score > topCapitals[i] && !placeLocated)
            {
                for (int j = 0; j < i; j++)
                {
                    topCapitals[j] = topCapitals[j + 1];
                    topNames[j] = topNames[j + 1];
                }
                topCapitals[i] = score;
                topNames[i] = "Player";
                placeLocated = true;
            }
        }
    }
    public void HighScore()
    {
        for (int i = 0; i < 10; i++)
        {
            gridNames.GetChild(i).GetComponent<Text>().text = topNames[9 - i] + ": ";
            grid.GetChild(i).GetComponent<Text>().text = "$ " + topCapitals[9 - i];
        }
    }
}
