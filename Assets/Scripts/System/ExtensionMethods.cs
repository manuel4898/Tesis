﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public static class ExtensionMethods
{
    public static int Pop(this List<int> collection, int index)
    {
        if (collection.Count > 0)
        {
            int item = collection[index];
            collection.RemoveAt(index);
            return item;
        }
        else
            return 0;
    }
}
