﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Manager : MonoBehaviour
{
    [SerializeField] Items Items;
    void Awake()
    {
        if (Items.ItemsData.Count == 0)
        {
            int[] baseValues = new int[]
            {
                25, 30, 45, 50, 45, 30, 75, 75, // Ingredientes
                250, 300, 450, 500, 650, 750, 800, 800, // Ingredientes
                35, 40, 60, 65, 65, 40, 100, 100, 950 // Productos
            };

            string[] names = new string[]
            {
                "Red Powder", "Orange Powder", "Purple Powder", "Green Powder", "Poisonous Mushroom", "Magic Tree Bark", "Unicorn Milk", "Water Soul Mushroom", // Ingredientes
                "Phoenix Soul Stone", "Magma Tree Branch", "Slime Core", "Dragon Scale", "Sacred Tree Fruit", "Griffin Egg", "Void Beetroot", "Void Fruit", // Ingredientes
                "Healing Potion", "Fire Immunity Potion", "Fortify Potion", "Strength Potion", "Poison", "Mystery Potion", "Purifying Potion", "Water Breathing Potion", "Flying Potion" // Productos
            };
            //normal demanda oferta = 5
            string[] demanda = new string[]
            {   
                "676545676545", "666555444555", "444555666555", "444555666555", "676545676545", "555555555555", "444666666444", "444555666555", // Ingredientes
                "676545676545", "666555444555", "444555666555", "444555666555", "555555555555", "444666666444", "666444444666", "666444444666", // Ingredientes
                "676545676545", "666555444555", "444555666555", "444555666555", "676545676545", "555555555555", "444666666444", "444555666555", "555555555555" // Productos
            };
            string[] oferta = new string[]
            {
                "555555555555", "666666444444", "666444444666", "666444444666", "555555555555", "555555555555", "444666666444", "444555666555", // Ingredientes
                "555555555555", "666666444444", "666444444666", "666444444666", "444459954444", "733333377777", "555555555555", "666664444446", // Ingredientes
                "555555555555", "666666444444", "666444444666", "666444444666", "555555555555", "555555555555", "444666666444", "444555666555", "444666666444" // Productos
            };

            //Rellena los sprites con los assets
            List<Sprite> sprites = new List<Sprite>();

            bool flag = true;
            int index = 0;
            int localIndex = 0;
            while (flag)
            {
                string j = "";
                if (localIndex < 10)
                    j = "0";
                j += localIndex.ToString();
                sprites.Add(Resources.Load<Sprite>("Sprites/Items/Ingredients/I" + j));
                if (sprites[index] == null)
                {
                    sprites.RemoveAt(index);
                    flag = false;
                }
                index++;
                localIndex++;
                if (localIndex > 99) flag = false;
            }
            flag = true;
            index = sprites.Count;
            localIndex = 0;
            while (flag)
            {
                string j = "";
                if (localIndex < 10)
                    j = "0";
                j += localIndex.ToString();
                sprites.Add(Resources.Load<Sprite>("Sprites/Items/Product/P" + j));
                if (sprites[index] == null)
                {
                    sprites.RemoveAt(index);
                    flag = false;
                }
                index++;
                localIndex++;
                if (localIndex > 99) flag = false;
            }
            flag = true;
            index = sprites.Count;
            localIndex = 0;
            while (flag)
            {
                string j = "";
                if (index < 10)
                    j = "0";
                j += localIndex.ToString();
                sprites.Add(Resources.Load<Sprite>("Sprites/Items/Companies/C" + j));
                if (sprites[index] == null)
                {
                    sprites.RemoveAt(index);
                    flag = false;
                }
                index++;
                localIndex++;
                if (localIndex > 99) flag = false;
            }

            //for (int k = 0; k < baseValues.Length; k++)
            //{
            //    string path = "Assets/Scriptable Objects/Items/" + k.ToString() + ".asset";
            //    ItemData item = ItemData.CreateInstance(k, names[k], sprites[k], baseValues[k], oferta[k], demanda[k]);
            //    AssetDatabase.CreateAsset(item, path);
            //    Items.ItemsData.Add(item);
            //}
        }


    }

    private void Start()
    {
        if (Items.ItemsData[0].baseValue == 0)
        {
            int[] baseValues = new int[]
            {
                25, 30, 45, 50, 45, 30, 75, 75, // Ingredientes
                250, 300, 450, 500, 650, 750, 800, 800, // Ingredientes
                35, 40, 60, 65, 65, 40, 100, 100, 950 // Productos
            };
            for (int k = 0; k < Items.ItemsData.Count; k++)
            {
                Items.ItemsData[k].baseValue = baseValues[k];
            }
        }
    }
    void FuncionesTemporalesNoUsadas()
    {
        /* Asignando sprites al singleton de items para luego jalarlos de ahí
        bool flag = true;
        int index = 0;
        int localIndex = 0;
        while (flag)
        {
            string j = "";
            if (localIndex < 10)
                j = "0";
            j += localIndex.ToString();
            sprites.Add(Resources.Load<Sprite>("Sprites/Items/Ingredients/I" + j));
            if (Items.Sprites[index] == null)
            {
                Items.Sprites.RemoveAt(index);
                flag = false;
            }
            index++;
            localIndex++;
            if (localIndex > 99) flag = false;
        }
        flag = true;
        index = Items.Sprites.Count;
        localIndex = 0;
        while (flag)
        {
            string j = "";
            if (localIndex < 10)
                j = "0";
            j += localIndex.ToString();
            Items.Sprites.Add(Resources.Load<Sprite>("Sprites/Items/Product/P" + j));
            if (Items.Sprites[index] == null)
            {
                Items.Sprites.RemoveAt(index);
                flag = false;
            }
            index++;
            localIndex++;
            if (localIndex > 99) flag = false;
        }
        flag = true;
        index = Items.Sprites.Count;
        localIndex = 0;
        while (flag)
        {
            string j = "";
            if (index < 10)
                j = "0";
            j += localIndex.ToString();
            Items.Sprites.Add(Resources.Load<Sprite>("Sprites/Items/Companies/C" + j));
            if (Items.Sprites[index] == null)
            {
                Items.Sprites.RemoveAt(index);
                flag = false;
            }
            index++;
            localIndex++;
            if (localIndex > 99) flag = false;
        }*/
    }
}
