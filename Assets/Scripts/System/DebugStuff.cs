﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugStuff : MonoBehaviour
{
    void Start()
    {

    }
    public void ButtonTest()
    {
        List<int> shops = new List<int>() {1,2,3,4,5};
        List<int> demanda = new List<int>() {0,0,1,1,2,2,3,4,5,6,7,7};
        List<int> removeIndexes = new List<int>();
        foreach (int index in demanda)
        {
            bool isPresent = false;
            foreach (int shop in shops)
            {
                if (shop == index) isPresent = true;
            }
            if (isPresent) removeIndexes.Add(index);
        }
        foreach (int index in removeIndexes)
        {
            demanda.Remove(index);
        }
        foreach (int id in demanda)
        {
            Debug.Log(id);
        }
    }
}
