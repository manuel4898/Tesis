﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DayManager : MonoBehaviour
{
    public Items Items;

    public Sprite[] stagesImage;
    public int stagesIndex;
    public Image clock;
    public Image buttonColor;

    public UnityEvent onNewDay = new UnityEvent();

    public UnityEvent onNewMonth = new UnityEvent();

    public GameObject[] panelBlockers;

    public void OnNewTime()
    {
        if (++stagesIndex > 3)
        {
            stagesIndex = 0;
            OnNewDay();
        }
        switch (stagesIndex)
        {
            case 0:
                buttonColor.color = new Color(1, 0.5f, 0);
                break;
            case 1:
                buttonColor.color = Color.yellow;
                break;
            case 2:
                buttonColor.color = new Color(1, 0.5f, 0);
                break;
            case 3:
                buttonColor.color = Color.blue;
                break;
        }
        clock.sprite = stagesImage[stagesIndex];
    }

    public void OnNewDay()
    {
        if (Items.day < 30)
        {
            Items.day++;
            onNewDay.Invoke();
        }
        else
        {
            Items.day = 0;
            OnNewMonth();
            OnNewDay();
        }
    }
    void OnNewMonth()
    {
        if (Items.month < 12)
        {
            Items.month++;
            onNewMonth.Invoke();
        }
        else
        {
            Items.month = 0;
            Items.day = 0;
            OnNewYear();
            OnNewMonth();
        }
    }
    void OnNewYear()
    {
        Items.year++;
    }

    public void BotonCloseDePaneles()
    {
        if (stagesIndex == 0)
        {
            foreach (var item in panelBlockers)
            {
                item.SetActive(true);
            }
        }
    }
}
