﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CargarEscena : MonoBehaviour
{
    public int SceneId;
    public GameObject loading;
    void Start()
    {
        loading.SetActive(false);
    }
    void LoadScene()
    {
        loading.SetActive(true);
        SceneManager.LoadScene(SceneId);
    }
    public void ActivateLoadingCanvas()
    {
        loading.SetActive(true);
        StartCoroutine(LoadLevelWithRealProcess());
    }
    IEnumerator LoadLevelWithRealProcess()
    {
        yield return new WaitForSecondsRealtime(1.0f);
        AsyncOperation async = SceneManager.LoadSceneAsync(SceneId);
        while (!async.isDone)
        {
            yield return null;
        }
    }
    public void Quit()
    {
        Application.Quit();
    }
}
