﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    int tutorialState;
    public GameObject tutorialBlocker;
    public GameObject[] iconPanelsButtons;
    public string[] tutorialTexts;

    public Text tutorialTextDisplay;
    private void Start()
    {
        tutorialTexts = new string[] {
            "You can use this panel to buy and sell products.",
            "Use this one to craft.",
            "Here you can see how you are doing compared to others.",
            "In this panel, you can invest. Try to predict accurately.",
            "Here you can read the news!",
            "Remember, each day you'll be charged a small portion of your debt.",
            "And each month, you'll be charged a big portion of your debt!",
            "If you lose all your money, you lose!",
            "So don't forget to save money.",
            "Good luck!"
        };
    }
    public void TutorialNext()
    {
        if(tutorialState < tutorialTexts.Length)
        {
            if(tutorialState < iconPanelsButtons.Length) iconPanelsButtons[tutorialState].SetActive(true);
            tutorialTextDisplay.text = tutorialTexts[tutorialState];
            tutorialState++;
        }
        else
        {
            this.gameObject.SetActive(false);
            tutorialBlocker.SetActive(false);
        }
    }
}
