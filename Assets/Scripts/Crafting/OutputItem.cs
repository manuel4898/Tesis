﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OutputItem : MonoBehaviour
{
    [SerializeField] Items items;
    public int id;
    [SerializeField] Inventory inventario;
    public InputItem inItem;
    public void Craft()
    {
        if (id != -1)
        {
            if (inItem.amountIn <= inventario.inventory[inItem.id])
            {
                inventario.Remove(inItem.amountIn, inItem.id);
                inventario.Add(inItem.amountOut, id);
            }
            this.GetComponent<Image>().enabled = false;
            this.id = -1;
            inItem.GetComponent<Image>().enabled = false;
            inItem.id = -1;
        }
    }
}
