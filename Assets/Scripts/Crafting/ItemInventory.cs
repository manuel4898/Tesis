﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemInventory : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] Items items;
    public int id;
    public Transform slot;
    [SerializeField] Inventory inventario;

    public InputItem inItem;
    void Start()
    {
        this.GetComponent<Image>().sprite = items.ItemsData[id].icon;
        this.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = "";
    }

    void Update()
    {
        this.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = inventario.inventory[id].ToString();
    }
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        inItem.GetComponent<Image>().enabled = true;
        inItem.GetComponent<Image>().sprite = items.ItemsData[id].icon;
        inItem.id = id;

        if (id + 16 < 24)
        {
            inItem.outItem.GetComponent<Image>().enabled = true;
            inItem.outItem.GetComponent<Image>().sprite = items.ItemsData[id + 16].icon;
            inItem.outItem.id = id + 16;
        }
        else
        {
            inItem.outItem.GetComponent<Image>().enabled = false;
            inItem.outItem.GetComponent<Image>().sprite = null;
            inItem.outItem.id = -1;
        }
    }
}
