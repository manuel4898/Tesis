﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSlotsHolder : MonoBehaviour
{
    [SerializeField] Transform slotsHolder;
    public Transform[] slots;
    public ItemInventory[] items;
    private void Awake()
    {
        items = this.transform.GetComponentsInChildren<ItemInventory>();
        slots = slotsHolder.GetComponentsInChildren<Transform>();
        for (int i = 0; i < items.Length; i++)
        {
            items[i].id = i;
            items[i].slot = slots[i];
        }
    }

}
