﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputItem : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] Items items;
    public int id = -1;
    [SerializeField] Inventory inventario;

    public OutputItem outItem;
    public int amountIn;
    public int amountOut;
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        if (id != -1)
        {
            this.GetComponent<Image>().enabled = false;
            this.id = -1;
            AmountInput("0");
            outItem.GetComponent<Image>().enabled = false;
        }
    }

    public void AmountInput(string amount)
    {
        if (!int.TryParse(amount, out this.amountIn)) this.amountIn = 0;
        else if (this.amountIn < 0) this.amountIn = 0;
        else if (this.amountIn > inventario.inventory[id]) this.amountIn = inventario.inventory[id];
        this.amountOut = this.amountIn; //temporal
        this.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = this.amountIn.ToString();
        outItem.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>().text = this.amountOut.ToString();
    }
}
