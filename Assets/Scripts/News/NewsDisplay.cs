﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewsDisplay : MonoBehaviour
{
    [SerializeField] News news;
    public Transform bNewsHolder;
    public Transform cNewsList;
    public Text[] newsText;

    public GameObject descriptionBox;
    private void OnEnable()
    {
        descriptionBox.SetActive(false);
        BreakingNews();
    }
    public void OnNewDay()
    {
        BreakingNews();
        CalendarDisplay();
    }
    public void BreakingNews()
    {
        newsText = bNewsHolder.GetComponentsInChildren<Text>();

        for (int i = 0; i < newsText.Length; i++)
        {
            newsText[i].text = "";
            newsText[i].transform.parent.GetComponent<Image>().enabled = false;
            newsText[i].transform.parent.GetComponent<NewButton>().descriptionBox = descriptionBox;
            if (i < news.breakingNews.Count)
            {
                newsText[i].text = news.breakingNews[i].name;
                newsText[i].transform.parent.GetComponent<NewButton>().descriptionToShow = news.breakingNews[i].description;
                newsText[i].transform.parent.GetComponent<Image>().enabled = true;
            }
        }
    }
    public void CalendarDisplay()
    {
        newsText = cNewsList.GetComponentsInChildren<Text>();
        for (int i = 0; i < newsText.Length; i++)
        {
            newsText[i].text = "";
            newsText[i].transform.parent.GetComponent<Image>().enabled = false;
            newsText[i].transform.parent.GetComponent<NewButton>().descriptionBox = descriptionBox;
            if (i < news.scheduledEvents[0].Count)
            {
                newsText[i].text = news.scheduledEvents[0][i].name;
                newsText[i].transform.parent.GetComponent<NewButton>().descriptionToShow = news.scheduledEvents[0][i].description;
                newsText[i].transform.parent.GetComponent<Image>().enabled = true;
            }
        }
    }

}

