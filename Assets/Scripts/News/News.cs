﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class New
{
    public string name;
    public int chance;
    public string description;
    public int duration;
    public int[] productsAfected;
    public int[] productsModifier;
    public int[] productsMaybeAfected;
    public int[] productsMaybeModifier;
}
[System.Serializable]
public class NewsList
{
    public New[] newsList;
}
public class News : MonoBehaviour
{
    public Items Items;
    //public Dictionary<int, Dictionary<int, int>> scheduledModifiers;
    //public Dictionary<int, Dictionary<int, List<New>>> scheduledEvents;

    public List<List<New>> scheduledEvents;

    public List<New> breakingNews;
    public TextAsset textJSON;
    public NewsList events = new NewsList();

    public Dropdown years;
    private void Start()
    {
        /*
        if (scheduledModifiers == null) scheduledModifiers = new Dictionary<int, Dictionary<int, int>>();
        if (!scheduledModifiers.ContainsKey(Items.year)) scheduledModifiers.Add(Items.year, new Dictionary<int, int>());
        if (scheduledEvents == null) scheduledEvents = new Dictionary<int, Dictionary<int, List<New>>>();
        if (!scheduledEvents.ContainsKey(Items.year)) scheduledEvents.Add(Items.year, new Dictionary<int, List<New>>());
        */
        if (scheduledEvents == null)
        {
            scheduledEvents = new List<List<New>>();
            scheduledEvents.Add(new List<New>());
        }

        if (breakingNews == null) breakingNews = new List<New>();
        events = JsonUtility.FromJson<NewsList>(textJSON.text);
    }

    public void OnNewDay()
    {
        breakingNews.Clear();
        if(scheduledEvents.Count > 0) scheduledEvents.RemoveAt(0);
        GenerateNews();
        foreach (ItemData item in Items.ItemsData)
        {
            item.NewDay(Items.month);
        }
    }

    public void GenerateNews()
    {
        List<int> eventChancePool = new List<int>();
        for (int i = 0; i < events.newsList.Length; i++)
        {
            New tempNew = events.newsList[i];
            for (int j = 0; j < tempNew.chance; j++)
            {
                eventChancePool.Add(i);
            }
        }
        int eventId = Random.Range(0, eventChancePool.Count);
        New newEvent = events.newsList[eventChancePool[eventId]];
        newEvent = SetMaybesInEvent(newEvent);
        breakingNews.Add(newEvent);
        AddEventToList(newEvent);
        AddEventToItems(newEvent);
    }
    New SetMaybesInEvent(New noticia)
    {
        List<int> temporalList = new List<int>();
        for (int i = 0; i < noticia.productsMaybeAfected.Length; i++)
        {
            temporalList.Add(i);
        }
        int affectedAmount = Random.Range(0, temporalList.Count + 1);
        for (int i = 0; i < affectedAmount; i++)
        {
            int affectedIndex = Random.Range(0, temporalList.Count);
            temporalList.RemoveAt(affectedIndex);
        }
        for (int i = 0; i < temporalList.Count; i++)
        {
            noticia.productsMaybeAfected[i] = -1;
        }
        int selectedMofifier = Random.Range(0, noticia.productsMaybeModifier.Length);
        for (int i = 0; i < noticia.productsMaybeModifier.Length; i++)
        {
            if (i != selectedMofifier) noticia.productsMaybeModifier[i] = -99;
        }
        return noticia;
    }
    void AddEventToList(New noticia)
    {
        for (int i = 0; i < noticia.duration; i++)
        {
            if (scheduledEvents.Count == i)
            {
                scheduledEvents.Add(new List<New>());
            }
            scheduledEvents[i].Add(noticia);
        }
    }
    void AddEventToItems(New noticia)
    {
        List<int> productsAfectedTrue = new List<int>();
        List<int> productsModifiersTrue = new List<int>();
        List<int> productsExtraTrue = new List<int>();

        //Crear nueva lista de productos incluyendo derivados del producto original
        for (int j = 0; j < noticia.productsAfected.Length; j++)
        {
            productsAfectedTrue.Add(noticia.productsAfected[j]);
            productsModifiersTrue.Add(noticia.productsModifier[j]);
            if (noticia.productsAfected[j] < 4)
            {
                productsAfectedTrue.Add(noticia.productsAfected[j] + 8);
                productsModifiersTrue.Add(noticia.productsModifier[j]);
            }
            if (noticia.productsAfected[j] < 8)
            {
                productsAfectedTrue.Add(noticia.productsAfected[j] + 16);
                productsModifiersTrue.Add(noticia.productsModifier[j]);
            }
        }
        foreach (int itemIndex in noticia.productsMaybeAfected)
        {
            if (itemIndex > -1)
            {
                productsExtraTrue.Add(itemIndex);
                if (itemIndex < 4) productsExtraTrue.Add(itemIndex + 8);
                if (itemIndex < 8) productsExtraTrue.Add(itemIndex + 16);
            }
        }

        int modifier = 0;
        foreach (int tempModifier in noticia.productsMaybeModifier)
        {
            if (tempModifier != -99) modifier = tempModifier;
        }

        //Aplicar modificadores a cada item
        for (int i = 0; i < noticia.duration; i++)
        {
            for (int j = 0; j < productsAfectedTrue.Count; j++)
            {
                if (this.Items.ItemsData[productsAfectedTrue[j]].dayTempAlter.Count == i) this.Items.ItemsData[productsAfectedTrue[j]].dayTempAlter.Add(0);
                this.Items.ItemsData[productsAfectedTrue[j]].dayTempAlter[i] += productsModifiersTrue[j];
            }
 
            for (int j = 0; j < productsExtraTrue.Count; j++)
            {
                if (this.Items.ItemsData[productsExtraTrue[j]].dayTempAlter.Count == i) this.Items.ItemsData[productsExtraTrue[j]].dayTempAlter.Add(0);
                this.Items.ItemsData[productsExtraTrue[j]].dayTempAlter[i] += modifier;
            }
        }
    }

    public void TestNews()
    {
        GenerateNews();
    }
    public void TestNewDay()
    {
        breakingNews.Clear();
        scheduledEvents.RemoveAt(0);
        GenerateNews();
        foreach (ItemData item in Items.ItemsData)
        {
            item.NewDay(Items.month);
        }
    }
    public void ResetNews()
    {
        foreach (ItemData item in Items.ItemsData)
        {
            item.dayTempAlter.Clear();
        }
        breakingNews.Clear();
        scheduledEvents.Clear();
        scheduledEvents.Add(new List<New>());
    }
}
