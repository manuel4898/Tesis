﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewButton : MonoBehaviour
{
    public string descriptionToShow;
    public GameObject descriptionBox;
    
    public void OnClick()
    {
        descriptionBox.SetActive(true);
        descriptionBox.GetComponentInChildren<Text>().text = descriptionToShow;
    }
}
