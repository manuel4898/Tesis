﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinDeMes : MonoBehaviour
{
    public Text money;
    public Text deudaRestanteText;
    public Inventory inventory;
    float deudaTotalOriginal;
    float deudaRestante;
    float cuotaDiaria;
    float cuotaMensual;

    public GameObject derrota;
    public GameObject victoria;
    void Start()
    {
        deudaTotalOriginal = 85700;
        deudaRestante = deudaTotalOriginal;
        deudaRestanteText.text = "Debt: " + deudaRestante.ToString();
        cuotaDiaria = 100;
        cuotaMensual = 4100;
    }
    private void Update()
    {
        money.text = "Money: " + inventory.money.ToString("F2");
    }
    public void OnNewDay()
    {
        deudaRestante -= cuotaDiaria;
        inventory.money -= cuotaDiaria;
        if (inventory.money < 0) GameOver();
        if (deudaRestante <= 0) Victoria();
        deudaRestanteText.text = "Deuda: " + deudaRestante.ToString();
    }
    public void OnNewMonth()
    {
        deudaRestante -= cuotaMensual;
        inventory.money -= cuotaMensual;
        if (inventory.money < 0) GameOver();
        if (deudaRestante <= 0) Victoria();
        deudaRestanteText.text = "Deuda: " + deudaRestante.ToString();
    }

    public void GameOver()
    {
        derrota.SetActive(true);
    }
    public void Victoria()
    {
        victoria.SetActive(true);
    }
}
