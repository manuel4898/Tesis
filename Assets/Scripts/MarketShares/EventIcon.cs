﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EventIcon : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] Items items;
    public int id;
    public Graph grafico;
    [SerializeField] Inventory inventario;

    private void Start()
    {
        Show();
    }
    public void Show()
    {
        if (id <= 24)
        {
            this.GetComponent<Image>().enabled = true;
            this.GetComponent<Image>().sprite = items.ItemsData[id].icon;
            this.transform.GetChild(0).gameObject.GetComponent<Text>().enabled = true;
            this.transform.GetChild(1).gameObject.GetComponent<Text>().enabled = true;
            this.transform.GetChild(0).gameObject.GetComponent<Text>().text = items.ItemsData[id].displayName;
        }
        else
        {
            this.GetComponent<Image>().sprite = null;
            this.GetComponent<Image>().enabled = false;
            this.transform.GetChild(0).gameObject.GetComponent<Text>().enabled = false;
            this.transform.GetChild(1).gameObject.GetComponent<Text>().enabled = false;
        }
    }

    private void Update()
    {
        if (id <= 24)
        {
            this.transform.GetChild(1).gameObject.GetComponent<Text>().text = inventario.shares[id].ToString();
        }
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        grafico.NewItemPrices(items.ItemsData[id].prices, id);
        inventario.id = id;
    }
}
