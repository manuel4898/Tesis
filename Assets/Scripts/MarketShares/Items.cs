﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Singletons/Items")]
public class Items : ScriptableObject
{
    [SerializeField] private List<ItemData> _itemsData;
    public int year;
    public int month;
    public int day;
    public List<ItemData> ItemsData { get { return _itemsData;} }

}
