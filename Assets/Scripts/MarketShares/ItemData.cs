﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName ="Inventory Item Data")]
public class ItemData : ScriptableObject
{
    public int id;
    public string displayName;
    public Sprite icon;
    public int baseValue;

    public float[] prices = new float[90];

    public int[] demanda = new int[12];
    public int[] oferta = new int[12];

    public List<int> dayTempAlter;
    public void Init(int id, string displayName, Sprite icon, int baseValue, string oferta, string demanda)
    {
        this.id = id;
        this.displayName = displayName;
        this.icon = icon;
        this.baseValue = baseValue;
        this.dayTempAlter = new List<int>();
        for (int i = 0; i < 12; i++)
        {
            char a = demanda[i];
            this.demanda[i] = int.Parse(a.ToString()) - 5;
            char b = oferta[i];
            this.oferta[i] = int.Parse(b.ToString()) - 5;
        }
        float price = baseValue;
        for (int i = 0; i < 90; i++)
        {
            float mappedValue = (demanda[i / 30] - oferta[i / 30]);
            mappedValue = (mappedValue - -5) / (5 - -5) * (2 - -2) + -2;
            price += Random.Range(-baseValue / 100f, baseValue / 100f) + Random.Range(0, baseValue / 100f) * mappedValue;
            if (price <= 0) price = Random.Range(0, baseValue / 20f);
            prices[i] = Mathf.Round(price * 100f) / 100f;
        }

    }
    public static ItemData CreateInstance(int id, string displayName, Sprite icon, int baseValue, string oferta, string demanda)
    {
        var data = ScriptableObject.CreateInstance<ItemData>();
        data.Init(id, displayName, icon, baseValue, oferta, demanda);
        return data;
    }

    public void NewDay(int mesActual)
    {
        float price = prices[89];
        price += Random.Range(-baseValue / 100f, baseValue / 100f) + Random.Range(0, baseValue / 100f) * (demanda[mesActual] - oferta[mesActual]) + Random.Range(0, baseValue / 100f) * dayTempAlter.Pop(0);
        for (int i = 0; i < prices.Length; i++)
        {
            if (i < prices.Length - 1)
                prices[i] = prices[i + 1];
            else
                prices[i] = Mathf.Round(price * 100f) / 100f;
        }
    }
}
