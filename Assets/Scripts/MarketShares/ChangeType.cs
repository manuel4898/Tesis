﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeType : MonoBehaviour
{
    [SerializeField] Transform[] itemLists;

    EventIcon[][] itemIds;

    public void OnClick(int type)
    {
        itemIds = new EventIcon[itemLists.Length][];
        for (int i = 0; i < itemLists.Length; i++)
        {
            itemIds[i] = itemLists[i].GetComponentsInChildren<EventIcon>();
        }
        for (int i = 0; i < 16; i++)
        {
            itemIds[i / 4][i % 4].id = i + type * 16;
            itemIds[i / 4][i % 4].Show();
        }
    }
}
