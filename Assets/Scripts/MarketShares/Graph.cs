﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Graph : MonoBehaviour
{
    [SerializeField] Items Items;
    [SerializeField] private Sprite circleSprite;
    private RectTransform graphContainer;
    private RectTransform labelTemplateX;
    private RectTransform labelTemplateY;
    private RectTransform dashTemplateX;
    private RectTransform dashTemplateY;

    private List<GameObject> gameObjectList;
    private List<float> valueList;
    private int maxVisibleValueAmmount = 7;

    public Text priceBuy;
    public Text priceSell;
    public int currentId;
    private void Start()
    {
        graphContainer = transform.Find("GraphCont").GetComponent<RectTransform>();
        labelTemplateX = graphContainer.Find("LabelTemplateX").GetComponent<RectTransform>();
        labelTemplateY = graphContainer.Find("LabelTemplateY").GetComponent<RectTransform>();
        dashTemplateX = graphContainer.Find("dashTemplateX").GetComponent<RectTransform>();
        dashTemplateY = graphContainer.Find("dashTemplateY").GetComponent<RectTransform>();

        gameObjectList = new List<GameObject>();

        valueList = new List<float>();

        for (int i = 0; i < 90; i++)
        {
            valueList.Add(Items.ItemsData[0].prices[i]);
        }
        ShowGraph(valueList);
        //CreateCircle(new Vector2(200, 200));
        ShowGraph(valueList);
    }

    private GameObject CreateCircle(Vector2 anchoredPosition)
    {
        GameObject gameObject = new GameObject("circle", typeof(Image));
        gameObject.transform.SetParent(graphContainer.GetChild(2), false);
        gameObject.GetComponent<Image>().sprite = circleSprite;
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = anchoredPosition;
        rectTransform.sizeDelta = new Vector2(11, 11);
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        return gameObject;
    }

    public void ShowGraph(List<float> valueList)
    {
        priceBuy.text = "Buy: $" + (valueList[valueList.Count - 1] * 1.05f).ToString("F1");
        priceSell.text = "Sell: $" + (valueList[valueList.Count - 1] * 0.95f).ToString("F1");
        foreach (GameObject game in gameObjectList)
        {
            Destroy(game);
        }
        gameObjectList.Clear();

        float graphHeight = graphContainer.sizeDelta.y;
        float graphWidth = graphContainer.sizeDelta.x;

        float yMaximum = valueList[valueList.Count - maxVisibleValueAmmount];
        float yMinimum = valueList[valueList.Count - maxVisibleValueAmmount];

        for (int i = valueList.Count - maxVisibleValueAmmount; i < valueList.Count; i++)
        {
            float value = valueList[i];
            if (value > yMaximum) yMaximum = value;
            if (value < yMinimum) yMinimum = value;
        }

        float middle = (yMaximum + yMinimum) / 2;

        yMaximum = Mathf.Round(yMaximum * 1.10f);
        yMinimum = Mathf.Round(yMinimum * 0.90f);

        /*
        yMaximum = yMaximum + middle * 0.2f;
        yMinimum = Mathf.Max(0, yMinimum - middle * 0.2f);


        int yTemp = 0;

        if (yMaximum < 800)
            yTemp = 100;
        else
            yTemp = 1000;

        yTemp = (int)(yMaximum / yTemp) * yTemp + yTemp;
        yMaximum = (float)(yTemp);*/


        float xSize = graphWidth / maxVisibleValueAmmount;

        int separatorCount = 10;

        for (int i = 0; i <= separatorCount; i++)
        {
            float normalizedValue = i * 1f / separatorCount;

            RectTransform labelY = Instantiate(labelTemplateY);
            labelY.SetParent(graphContainer.GetChild(1), false);
            labelY.gameObject.SetActive(true);
            labelY.anchoredPosition = new Vector2(-40.5f, normalizedValue * graphHeight - 7.5f);
            labelY.GetComponent<Text>().text = "$" + (normalizedValue * (yMaximum - yMinimum) + yMinimum).ToString("F1");
            gameObjectList.Add(labelY.gameObject);

            RectTransform dashY = Instantiate(dashTemplateY);
            dashY.SetParent(graphContainer.GetChild(1), false);
            dashY.gameObject.SetActive(true);
            dashY.anchoredPosition = new Vector2(0, normalizedValue * graphHeight);
            gameObjectList.Add(dashY.gameObject);

        }

        int xIndex = 0;
        GameObject lastCircleGameObject = null;
        for (int i = valueList.Count - maxVisibleValueAmmount; i < valueList.Count; i++)
        {
            float xPosition = xIndex * xSize;
            float yPosition = ((valueList[i] - yMinimum)/ (yMaximum - yMinimum)) * graphHeight;

            if (maxVisibleValueAmmount == 7 || (maxVisibleValueAmmount == 30 && (xIndex + 1) % 5 == 0))
            {
                RectTransform dashX = Instantiate(dashTemplateX);
                dashX.SetParent(graphContainer.GetChild(1), false);
                dashX.gameObject.SetActive(true);
                dashX.anchoredPosition = new Vector2(xPosition, 0);
                gameObjectList.Add(dashX.gameObject);

                RectTransform labelX = Instantiate(labelTemplateX);
                labelX.SetParent(graphContainer.GetChild(1), false);
                labelX.gameObject.SetActive(true);
                labelX.anchoredPosition = new Vector2(xPosition - 17.5f, -17.5f);
                labelX.GetComponent<Text>().text = (xIndex + 1).ToString();
                gameObjectList.Add(labelX.gameObject);
            }


            GameObject circleGameObject = CreateCircle(new Vector2(xPosition, yPosition));
            circleGameObject.AddComponent<EventCircle>();
            circleGameObject.GetComponent<EventCircle>().label = Instantiate(labelTemplateY);
            circleGameObject.GetComponent<EventCircle>().label.SetParent(graphContainer.GetChild(1), false);
            circleGameObject.GetComponent<EventCircle>().label.GetComponent<RectTransform>().anchoredPosition = new Vector2(430f, yPosition - 7.5f);
            circleGameObject.GetComponent<EventCircle>().label.GetComponent<Text>().text = "$" + valueList[i].ToString("F1");
            circleGameObject.GetComponent<EventCircle>().dash = Instantiate(dashTemplateY);
            circleGameObject.GetComponent<EventCircle>().dash.SetParent(graphContainer.GetChild(1), false);
            circleGameObject.GetComponent<EventCircle>().dash.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, yPosition);

            gameObjectList.Add(circleGameObject);
            if (lastCircleGameObject != null)
            {
                GameObject dotConnectionObject = CreateDotConnection(lastCircleGameObject.GetComponent<RectTransform>().anchoredPosition, circleGameObject.GetComponent<RectTransform>().anchoredPosition);
                gameObjectList.Add(dotConnectionObject);
            }
            lastCircleGameObject = circleGameObject;

            /*
            RectTransform labelX = Instantiate(labelTemplateX);
            labelX.SetParent(graphContainer, false);
            labelX.gameObject.SetActive(true);
            labelX.anchoredPosition = new Vector2(xPosition, -17.5f);
            labelX.GetComponent<Text>().text = i.ToString();

            RectTransform dashX = Instantiate(dashTemplateX);
            dashX.SetParent(graphContainer, false);
            dashX.gameObject.SetActive(true);
            dashX.anchoredPosition = new Vector2(xPosition, 0);
            */

            xIndex++;
        }

    }

    private GameObject CreateDotConnection(Vector2 dotPositionA, Vector2 dotPositionB)
    {
        GameObject gameObject = new GameObject("dotConnection", typeof(Image));
        gameObject.transform.SetParent(graphContainer.GetChild(1), false);
        gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        Vector2 dir = (dotPositionB - dotPositionA).normalized;
        float distance = Vector2.Distance(dotPositionA, dotPositionB);
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        rectTransform.sizeDelta = new Vector2(distance, 3f);
        rectTransform.anchoredPosition = dotPositionA + dir * distance * 0.5f;
        rectTransform.localEulerAngles = new Vector3(0, 0, (Mathf.Atan2(dir.y, dir.x) * 180 / Mathf.PI));
        return gameObject;
    }

    public void ChangeDaysShown(int days)
    {
        maxVisibleValueAmmount = days;
        ShowGraph(valueList);
    }
    public void OnNewDay()
    {
        if(valueList != null) NewItemPrices(Items.ItemsData[currentId].prices, currentId);
    }

    public void NewItemPrices(float[] itemValueList, int id)
    {
        if (valueList != null) valueList.Clear();
        foreach (float item in itemValueList)
        {
            valueList.Add(item);
        }
        currentId = id;
        ShowGraph(valueList);
    }
}

public class EventCircle: MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public RectTransform label;
    public RectTransform dash;

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        label.gameObject.SetActive(true);
        dash.gameObject.SetActive(true);
    }
    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        label.gameObject.SetActive(false);
        dash.gameObject.SetActive(false);
    }
}
